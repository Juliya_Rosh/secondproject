package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 23.02.2017.
 */
public class DifferentNumbers {
    public  void differenttN() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int z = -1;
        System.out.println("Введите число > 0");
        int n = Integer.parseInt(reader.readLine());
        int[] a = new int[n];
        System.out.println("Введите " + n + " чисел");
        for (int y = 0;y<n;y++){
            int x = Integer.parseInt(reader.readLine());
            a[y]= x; // заполняем массив
            if (z<0) {if (s4etUn(x)>0)z=y;} // если подходящее число еще не найдено, запускаем проверку
        }
        if (z<0)System.out.println("Чисел соответствующих условию нет"); // выводим результат на экран
        else System.out.println(a[z]);
    }

    public static int s4etUn(int x){ // метод проверки уникальных цифр в числе
        int i, j = 0, m = x;
        for (i = 0; ; i++) { // вычисляем количество цифр в числе
            if (m != 0) m = m / 10;
            else { m = x; break; } // возвращаем значение для следующего fora, чтобы не плодить переменные
        }
        int[] a = new int[i]; // создаем массив по количеству цифр в числе
        for (int y = a.length; y > 0; y--) {
            a[y - 1] = m % 10; // заполняем массив
            m = m / 10;
        }
        for (int y = a.length - 1; y > 0; y--) { // сортируем массив
            for (int z = 0; z < y; z++) {
                if (a[z] > a[z + 1]) {
                    int w = a[z];
                    a[z] = a[z + 1];
                    a[z + 1] = w;
                }
            }
        }
        for (int y = 0; y < a.length;y++) { // проверям уникальность цифр
            if (y == a.length - 1)j=1;
            else{ if (a[y] == a[y + 1]) break; }
        }
        return j;
    }
}
