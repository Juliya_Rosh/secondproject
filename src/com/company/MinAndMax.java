package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by User1 on 21.02.2017.
 */
public class MinAndMax {
    public void minMax () throws IOException {
        System.out.println("Введите любое количество целых чисел через пробел:");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        List<String> args = Arrays.asList(bufferedReader.readLine().split(" "));
        String min = args.get(0);
        String max = args.get(0);
        for (String arg : args) {
            arg = arg.replace("-", "");
            if (arg.length()< min.length()){
                min = arg;
            }
            if (arg.length()>max.length()){
                max = arg;
            }
        }

        System.out.println("Минимальное число: " + min + " Его длина: " + min.length());
        System.out.println("Максимальное число: " + max + " Его длина: " + max.length());


    }

}
