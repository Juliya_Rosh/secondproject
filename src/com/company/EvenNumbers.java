package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 24.02.2017.
 */
public class EvenNumbers {
    public void evenN() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int chet = 0;
        int chetNechet = 0;
        System.out.println("Введите число > 0");
        int n = Integer.parseInt(reader.readLine());
        int[] a = new int[n];
        int[] b = new int[n];
        System.out.println("Введите " + n + " чисел");

        for (int y = 0;y<n;y++) {
            int x = Integer.parseInt(reader.readLine());
            a[y] = x;
            double z = (double) x/2;
            if ((z-(int)z)==0){ b[chet]=x;chet++;}
        }
        for (int y = 0;y<b.length;y++){
            if (s4et(b[y])==1)chetNechet++;
        }
        System.out.println("Количество четных чисел: "+chet);
        System.out.println("из них чисел с равным количеством четных и нечетных цифр: "+chetNechet);
    }
    public static int s4et(int x) {
        int chet = 0, nechet = 0;
        if(x==0)chet++;
        else{
            for (; x > 0; ) {
                int n = x%10;
                double o= (double) n/2;
                x = x / 10;
                if ((o-(int)o)==0)chet++;
                else nechet++;
            }
        }
        if (chet==nechet)return 1;
        else return 0;
    }
}
