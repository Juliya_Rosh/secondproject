package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 01.03.2017.
 */
public class MatrixRandom {
    public void matrixR () throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число > 1");
        int n = Integer.parseInt(reader.readLine());
        int min = 1, max = n*n;
        int[][] a = new int[n][n];

        for (int y = 0; y < n; y++){
            for (int z = 0; z < n; z++) {
                a[y][z] = random(min,max);
            }
        }

        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++){
                System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }

    public static int random(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

}
