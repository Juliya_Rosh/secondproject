package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 01.03.2017.
 */
public class ZeroElements {
    public static int n;
    public void zeroE() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ведите любое число меньше 0");
        int min = Integer.parseInt(reader.readLine());
        System.out.println("Ведите любое число больше 0");
        int max = Integer.parseInt(reader.readLine());
        System.out.println("Ведите число больше 1");
        n = Integer.parseInt(reader.readLine());
        int[][] a = new int[n][n];

        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++) {
                a[y][z] = random(min, max);
            }
        }
        System.out.println("Исходная матрица");
        print(a);
        System.out.println("Преобразованная матрица:");
        print(zero(copy(a)));

    }

    public static int[][] zero(int[][]a){
        int m = a.length;
        for (int y = 0; y < m; y++){
            int l = m;
            for (int z = 0; z < l;){
                if (a[y][z] == 0) {
                    for (int w = z; w < l - 1; w++) {
                        a[y][w] = a[y][w + 1];
                    }
                    a[y][l - 1] = 0;
                    l--;
                }
                else z++;
            }
        }
        return a;
    }

    public static int random(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public static void print(int[][] a){
        int m = a.length;
        for (int y = 0; y < m; y++) {
            for (int z = 0; z < m; z++){
                if(y == m-1 & z == m-1) System.out.println(a[y][z]);
                else System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }

    public static int [][] copy(int[][]a){
        int m = a.length;
        int[][] b = new int[m][m];
        for (int i = 0; i < m; i++) {
            System.arraycopy(a[i], 0, b[i], 0, n);
        }
        return b;
    }
}
