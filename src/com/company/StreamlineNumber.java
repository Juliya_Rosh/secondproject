package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by User1 on 22.02.2017.
 */
public class StreamlineNumber {
    public void streamlineN() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите количество чисел, которое вы будете вводить");
        int n = Integer.parseInt(reader.readLine());
        String[] a = new String[n];
        System.out.println("Введите " + n + " чисел через Enter");
        for (int y = 0; y < n; y++) {
            a[y] = reader.readLine();
        }
        for (int y = a.length - 1; y > 0; y--) {
            for (int z = 0; z < y; z++) {
                if (a[z].length() > a[z + 1].length()) {
                    String x = a[z];
                    a[z] = a[z + 1];
                    a[z + 1] = x;
                }
            }
        }
        System.out.println("По возрастанию:" + Arrays.toString(a));
        for (int y = a.length - 1; y > 0; y--) {
            for (int z = 0; z < y; z++) {
                if (a[z].length() < a[z + 1].length()) {
                    String x = a[z];
                    a[z] = a[z + 1];
                    a[z + 1] = x;
                }
            }
        }
        System.out.println("По убыванию:" + Arrays.toString(a));
    }
}
