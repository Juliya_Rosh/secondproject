package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 01.03.2017.
 */
public class ShiftMatrix {
    private static int n, k;

    public void shiftM() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число n > 1");
        n = Integer.parseInt(reader.readLine());
        System.out.println("Введите число k< n");
        k = Integer.parseInt(reader.readLine());
        int min = 1, max = n * n;
        int[][] a = new int[n][n];

        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++) {
                a[y][z] = random(min, max);
            }
        }
        System.out.println("Исходная матрица:");
        print(a);
        System.out.println("Сдвиг матрицы на " + k + " позиций вправо:");
        print(right(copy(a)));
        System.out.println("Сдвиг матрицы на " + k + " позиций влево:");
        print(left(copy(a)));
        System.out.println("Сдвиг матрицы на " + k + " позиций вверх:");
        print(up(copy(a)));
        System.out.println("Сдвиг матрицы на " + k + " позиций вниз:");
        print(down(copy(a)));
    }

    public static int random(int min, int max) {
        max -= min; // вычисляем диапазон
        return (int) (Math.random() * ++max) + min;
    }

    public static int[][] copy(int[][] a) {
        int[][] b = new int[n][n];
        for (int i = 0; i < n; i++) {
            System.arraycopy(a[i], 0, b[i], 0, n);
        }
        return b;
    }

    public static void print(int[][] a) {
        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++) {
                if (y == n - 1 & z == n - 1) System.out.println(a[y][z]);
                else System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }

    public static int[][] right(int[][] a) {
        for (int y = 0; y < n; y++) {
            int x = 0, k = ShiftMatrix.k;
            for (; k > 0; k--) {
                for (int z = n - 1; z >= 0; z--) {
                    if (z == n - 1) x = a[y][z];
                    else a[y][z + 1] = a[y][z];
                    if (z == 0) a[y][z] = x;
                }
            }
        }
        return a;
    }

    public static int[][] left(int[][] a) {
        for (int y = 0; y < n; y++) {
            int x = 0, k = ShiftMatrix.k;
            for (; k > 0; k--) {
                for (int z = 0; z < n; z++) {
                    if (z == 0) x = a[y][z];
                    else a[y][z - 1] = a[y][z];
                    if (z == n - 1) a[y][z] = x;
                }
            }
        }
        return a;
    }

    public static int[][] up(int[][] a){
        int k = ShiftMatrix.k;
        int [] x = new int[n];
        for (; k > 0; k--){
            for (int y = 0; y < n; y++){
                for (int z = 0; z < n; z++){
                    if (y == 0) x[z] = a[y][z];
                    else a[y-1][z] = a[y][z];
                    if (y == n - 1) a[y][z] = x[z];
                }
            }
        }
        return a;
    }

    public static int[][] down(int[][] a){
        int k = ShiftMatrix.k;
        int [] x = new int[n];
        for (; k > 0; k--){
            for (int y = n - 1; y >= 0; y--){
                for (int z = 0; z < n; z++){
                    if (y == n - 1) x[z] = a[y][z];
                    else a[y+1][z] = a[y][z];
                    if (y == 0) a[y][z] = x[z];
                }
            }
        }
        return a;
    }
}
