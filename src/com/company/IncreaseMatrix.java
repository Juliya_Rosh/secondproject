package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 01.03.2017.
 */
public class IncreaseMatrix {
    public static int n;
    public void increaseM() throws IOException {
        BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число > 1");
        n = Integer.parseInt(reader.readLine());
        int min = 1, max = n*n;
        int[][] a = new int[n][n];

        for (int y = 0; y < n; y++){
            for (int z = 0; z < n; z++) {
                a[y][z] = random(min,max);
            }
        }
        System.out.println("Исходная марица:");
        print(a);
        System.out.println("Строки в порядке возрастания:");
        print(line(copy(a)));
        System.out.println("Столбцы в порядке возрастания:");
        print(post(copy(a)));
    }

    public static int random(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public static int [][] copy(int[][]a){
        int[][] b = new int[n][n];
        for (int i = 0; i < n; i++) {
            System.arraycopy(a[i], 0, b[i], 0, n);
        }
        return b;
    }

    public static void print(int[][] a){
        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++){
                if(y == n-1 & z == n-1) System.out.println(a[y][z]);
                else System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }

    public static int[][] line(int[][] a) {
        for (int y = 0; y < n; y++){
            for (int z = n - 1; z > 0; z--) {
                for (int w = 0; w < z; w++) {
                    if (a[y][w] > a[y][w + 1]) {
                        int x = a[y][w];
                        a[y][w] = a[y][w + 1];
                        a[y][w + 1] = x;
                    }
                }
            }
        }
        return a;
    }

    public static int [][] post (int [][]a) {
        for (int y = n - 1; y > 0; y--){
            for (int z = 0; z < y; z++) {
                for (int w = 0; w < n; w++) {
                    if (a[z][w] > a[z+1][w]) {
                        int x = a[z][w];
                        a[z][w] = a[z+1][w];
                        a[z+1][w] = x;
                    }
                }
            }
        }
        return a;
    }
}
