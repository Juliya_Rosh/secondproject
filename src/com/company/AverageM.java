package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 01.03.2017.
 */
public class AverageM {
    public static int n;
    public static double []b;
    public void averageMatrix () throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ведите любое число меньше 0");
        int min = Integer.parseInt(reader.readLine());
        System.out.println("Ведите любое число больше 0");
        int max = Integer.parseInt(reader.readLine());
        System.out.println("Ведите число больше 1");
        n = Integer.parseInt(reader.readLine());
        double[][] a = new double[n][n];
        b = new double[n];

        for (int y = 0; y < n; y++){
            double [] x = new double[n];
            for (int z = 0; z < n; z++){
                a[y][z] = random(min, max);
                x[z] = a[y][z];
            }
            b[y] = average(x);
        }

        System.out.println("Ваша исходная матрица");
        print(a);

        System.out.println("Матрица полученная в результате вычитания среднего арифметического значения:");
        print(result(copy(a)));
    }
    public static double average(double []a){
        double sum = 0;
        for (int y = 0; y < n; y++){
            sum += a[y];
        }
        return sum/n;
    }

    public static double [][] result(double [][]a){
        for (int y = 0; y < n; y++){
            for (int z = 0; z < n; z++){
                a[y][z] = a[y][z] - b[y];
            }
        }
        return a;
    }

    public static int random(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public static void print(double[][] a){
        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++){
                if(y == n-1 & z == n-1) System.out.println(a[y][z]);
                else System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }

    public static double [][] copy(double[][]a){
        double[][] b = new double[n][n];
        for (int i = 0; i < n; i++) {
            System.arraycopy(a[i], 0, b[i], 0, n);
        }
        return b;
    }
}
