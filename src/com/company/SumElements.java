package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 01.03.2017.
 */
public class SumElements {
    public static int n;
    public void sumE () throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ведите любое число меньше 0");
        int min = Integer.parseInt(reader.readLine());
        System.out.println("Ведите любое число больше 0");
        int max = Integer.parseInt(reader.readLine());
        System.out.println("Ведите число больше 1");
        n = Integer.parseInt(reader.readLine());
        int [][] a = new int[n][n];

        for (int y = 0; y < n; y++){
            for (int z = 0; z < n; z++){
                a[y][z] = random(min, max);
            }
        }

        System.out.println("Ваша матрица");
        print(a);

        for (int y = 0; y < n; y++){
            int one = -1, two = -1, sum = 0;
            for (int z = 0; z < n; z++){
                if (a[y][z] > 0){
                    if (one < 0)one = z;
                    else two = z;
                }
                if (one >= 0 & two > 0){
                    for (int w = one + 1;w < two; w++){
                        sum += a[y][w];
                    }
                    System.out.println("Сумма элементов расположенных между 1 и 2 положительным числом строки " + (y+1) + ": "+sum);
                    break;
                }
                if (z == n - 1) {
                    if (one < 0) System.out.println("В строке " + (y + 1) + " нет положительных чисел");
                    else System.out.println("В строке " + (y + 1) + " одно положительное число");
                }
            }
        }
    }

    public static int random(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public static void print(int[][] a){
        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++){
                if(y == n-1 & z == n-1) System.out.println(a[y][z]);
                else System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }
}
