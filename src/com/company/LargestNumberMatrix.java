package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 01.03.2017.
 */
public class LargestNumberMatrix {
    public static int n;
    public void largestN() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ведите любое число меньше 0");
        int min = Integer.parseInt(reader.readLine());
        System.out.println("Ведите любое число больше 0");
        int max = Integer.parseInt(reader.readLine());
        System.out.println("Ведите число больше 1");
        n = Integer.parseInt(reader.readLine());
        int[][] a = new int[n][n];

        for (int y = 0; y < n; y++){
            for (int z = 0; z < n; z++){
                a[y][z] = random(min, max);
            }
        }
        System.out.println("Ваша матрица");
        print(a);
        System.out.println("Наибольшее число возрастающих элементов матрицы, идущих подряд: "+ up(a));
        System.out.println("Наибольшее число убывающих элементов матрицы, идущих подряд: "+ down(a));
    }

    public static int up(int[][]a){
        int max = 1, work = 1;
        for (int y = 0; y < n; y++){
            for (int z = 1; z< n; z++){
                if (a[y][z-1] < a[y][z])work++;
                else {
                    if (work > 1){
                        if (work > max){
                            max = work;
                            work = 1;
                        }
                        else work = 1;
                    }
                }
                if (z == n-1 & work > 1 & y != n-1){
                    if (a[y+1][0] > a[y][z])work++;
                    else{
                        if (work > max){
                            max = work;
                            work = 1;
                        }
                        else work = 1;
                    }
                }
                if (z == n-1 & y == n-1 & work > max)max = work;
            }
        }
        return max;
    }

    public static int down(int[][]a){
        int max = 1, work = 1;
        for (int y = 0; y < n; y++){
            for (int z = 1; z< n; z++){
                if (a[y][z-1] > a[y][z])work++;
                else {
                    if (work > 1){
                        if (work > max){
                            max = work;
                            work = 1;
                        }
                        else work = 1;
                    }
                }
                if (z == n-1 & work > 1 & y != n-1){
                    if (a[y+1][0] < a[y][z])work++;
                    else{
                        if (work > max){
                            max = work;
                            work = 1;
                        }
                        else work = 1;
                    }
                }
                if (z == n-1 & y == n-1 & work > max)max = work;
            }
        }
        return max;
    }

    public static int random(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public static void print(int[][] a){
        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++){
                if(y == n-1 & z == n-1) System.out.println(a[y][z]);
                else System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }
}
