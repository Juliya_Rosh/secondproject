package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 24.02.2017.
 */
public class Matrix {
    public static int k, n;
    public void matrixN() throws IOException {

        BufferedReader q = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число > 1");
        k = Integer.parseInt(q.readLine());
        if(Math.sqrt(k)-(int)Math.sqrt(k)==0)n = (int)Math.sqrt(k);
        else n = (int)Math.sqrt(k)+1;

        int[][] a = new int[n][n];
        int[][] b = new int[n][n];

        System.out.println("Заполнение матрицы слева направо:");
        print(line(a));
        System.out.println("Заполнение матрицы сверху вниз:");
        print(post(b));
    }

    public static void print(int[][] a){
        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++){
                if(y == n-1 & z == n-1) System.out.println(a[y][z]);
                else System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }

    public static int [][] line (int [][] a){
        int l = 1;
        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++) {
                if (l > k) break;
                else {
                    a[y][z] = l;
                    l++;
                }
            }
        }
        return a;
    }

    public static int [][] post (int [][] a){
        int l = 1;
        for (int y = 0; y < n; y++) {
            int p = l; l++;
            for (int z = 0; z < n; z++) {
                if (p > k) break;
                else a[y][z] = p; p = p + n;
            }
        }
        return a;
    }
}
