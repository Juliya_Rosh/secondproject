package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 01.03.2017.
 */
public class TurnMatrix {
    public static int n;
    public void turnM () throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ведите любое число меньше 0");
        int min = Integer.parseInt(reader.readLine());
        System.out.println("Ведите любое число больше 0");
        int max = Integer.parseInt(reader.readLine());
        System.out.println("Ведите число больше 1");
        n = Integer.parseInt(reader.readLine());
        int[][] a = new int[n][n];

        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++) {
                a[y][z] = random(min, max);
            }
        }
        System.out.println("Ваша матрица");
        print(a);
        System.out.println("Поворачиваем матрицу на 90 градусов против часовой стрелки:");
        print(turn(copy(a)));
        System.out.println("Поворачиваем матрицу на 180 градусов против часовой стрелки:");
        print(turn(turn(copy(a))));
        System.out.println("Поворачиваем матрицу на 270 градусов против часовой стрелки:");
        print(turn(turn(turn(copy(a)))));
    }

    public static int[][] turn(int[][]a){
        int w = 0;
        for (int y = n-1; y >= n/2; y--){
            int v = y;
            for (int z = w; z < y; z++){
                int x = a[w][z];
                a[w][z] = a[z][y];
                a[z][y] = a[y][v];
                a[y][v] = a[v][w];
                a[v][w] = x;
                v--;
            }
            w++;
        }
        return a;
    }


    public static int random(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public static void print(int[][] a){
        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++){
                if(y == n-1 & z == n-1) System.out.println(a[y][z]);
                else System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }

    public static int [][] copy(int[][]a){
        int[][] b = new int[n][n];
        for (int i = 0; i < n; i++) {
            System.arraycopy(a[i], 0, b[i], 0, n);
        }
        return b;
    }
}
