package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 23.02.2017.
 */
public class MinNumber {
    public void minN ()throws IOException

    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int min = Integer.MAX_VALUE;
        System.out.println("Введите число > 0");
        int n = 0;
        try {
            n = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int[] a = new int[n];
        System.out.println("Введите " + n + " чисел");
        for (int y = 0; y<a.length;y++){
            try {
                a[y]=Integer.parseInt(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (s4etUn(a[y])<min)min=s4etUn(a[y]);
        }

        for (int y = 0; ;y++){
            if (s4etUn(a[y])==min){
                System.out.println("Число с минимальным количеством уникальных цифр: "+a[y]);
                System.out.println("Количество уникальных цифр: "+min);
                break;
            }
        }
    }

    public static int s4etUn(int x){
        int i, j = 1, m = x;
        for (i = 0; ; i++) {
            if (m != 0) m = m / 10;
            else { m = x; break; }
        }
        int[] a = new int[i];
        for (int y = a.length; y > 0; y--) {
            a[y - 1] = m % 10;
            m = m / 10;
        }
        for (int y = a.length - 1; y > 0; y--) {
            for (int z = 0; z < y; z++) {
                if (a[z] > a[z + 1]) {
                    int w = a[z];
                    a[z] = a[z + 1];
                    a[z + 1] = w;
                }
            }
        }
        for (int y = 0; y < a.length - 1; y++) {
            if (a[y] != a[y + 1]) j++;
        }
        return j;
    }
}
