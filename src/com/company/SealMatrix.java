package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by User1 on 01.03.2017.
 */
public class SealMatrix {
    public static int n;
    public void sealM () throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ведите любое число меньше 0");
        int min = Integer.parseInt(reader.readLine());
        System.out.println("Ведите любое число больше 0");
        int max = Integer.parseInt(reader.readLine());
        System.out.println("Ведите число больше 1");
        n = Integer.parseInt(reader.readLine());
        int[][] a = new int[n][n];
        for (int y = 0; y < n; y++) {
            for (int z = 0; z < n; z++) {
                a[y][z] = random(min, max);
            }
        }
        System.out.println("Исходная матрица");
        print(a);
        System.out.println("Уплотненная матрица:");
        if (zero(copy(a)) != null)print(zero(copy(a)));
        else System.out.println("матрица не существует");
    }

    public static int[][] zero(int[][]a){
        int[] post = new int[a.length];
        int[] line = new int[a.length];
        int m = a.length;
        int p = m, l = m;
        for (int y = 0; y < m; y++){
            for (int z =0; z < m; z++) {
                if (a[y][z] == 0) {
                    post[z] = 1;
                    line[y] = 1;
                }
            }
        }
        for (int y = 0; y < m; y++){
            if (post[y] == 1)l--;
            if (line[y] == 1)p--;
        }
        if (l == 0 & p == 0)return null;
        else return deleteLine(deletePost(a),line);
    }
    public static int[][] deleteLine(int[][]a, int []line) {
        int m = a.length;
        int l = m;
        for (int y = m-1; y >= 0; y--) {
            if (line[y]==1) {
                if (y != m - 1) {
                    for (int w = y; w < l-1; w++) {
                        for (int z = 0; z < a[y].length; z++) {
                            a[w][z] = a[w + 1][z];
                        }
                    }
                }
                l--;
            }
        }
        return changeSize(a,l,a[0].length);
    }

    public static int[][] deletePost(int[][]a){
        int m = a.length;
        int l = m;
        for (int y = 0; y < m; y++) {
            for (int z = 0; z < l; ) {
                if (a[y][z] == 0) {
                    if (z != l-1){
                        for (int v = 0; v < m; v++) {
                            for (int w = z; w < l - 1; w++) {
                                a[v][w] = a[v][w + 1];
                            }
                        }
                    }
                    l--;
                } else z++;
            }
        }
        return changeSize(a,m,l);
    }

    public static int[][] changeSize(int[][] a, int p, int l) {
        int[][]b = new int[p][l];
        for (int y = 0; y < p; y++){
            for (int z = 0; z < l; z++){
                b[y][z] = a[y][z];
            }
        }
        return b;
    }

    public static int random(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public static void print(int[][] a){
        for (int y = 0; y < a.length; y++) {
            for (int z = 0; z < a[y].length; z++){
                if(y == a.length-1 & z == a[y].length-1) System.out.println(a[y][z]);
                else System.out.print(a[y][z] + " ");
            }
            System.out.print("\n");
        }
    }

    public static int [][] copy(int[][]a){
        int m = a.length;
        int[][] b = new int[m][m];
        for (int i = 0; i < m; i++) {
            System.arraycopy(a[i], 0, b[i], 0, n);
        }
        return b;
    }
}
