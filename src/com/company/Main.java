package com.company;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

	MinAndMax minAndMax = new MinAndMax();
	StreamlineNumber streamlineNumber = new StreamlineNumber();
	BringLenght bringLenght = new BringLenght();
	MinNumber minNumber = new MinNumber();
	NumbersIncrease numbersIncrease = new NumbersIncrease();
	DifferentNumbers differentNumbers = new DifferentNumbers();
	EvenNumbers evenNumbers = new EvenNumbers();
	Matrix matrix = new Matrix();
	MatrixRandom matrixRandom = new MatrixRandom();
	IncreaseMatrix increaseMatrix = new IncreaseMatrix();
	ShiftMatrix shiftMatrix = new ShiftMatrix();
	LargestNumberMatrix largestNumberMatrix = new LargestNumberMatrix();
	SumElements sumElements = new SumElements();
	TurnMatrix turnMatrix = new TurnMatrix();
	AverageM averageM = new AverageM();
	SealMatrix sealMatrix = new SealMatrix();
	ZeroElements zeroElements = new ZeroElements();




        System.out.println("====================================================================================");
        System.out.println("\nНайти самое короткое и самое длинное число. Вывести найденные числа и их длинну.\n");
        minAndMax.minMax();

        System.out.println("====================================================================================");
        System.out.println("\nУпорядочить и вывести числа в порядке возростания (убывания) значений их длины.\n");
        streamlineNumber.streamlineN();

        System.out.println("====================================================================================");
        System.out.println("\nВывести на консоль те числа, длина которых больше (меньше) средней, а также длину.\n");
       bringLenght.bringL();

        System.out.println("====================================================================================");
        System.out.println("\nНайти число, в котором число различных цифр минимально. Если таких чисел несколько, найти первое из них.\n");
       minNumber.minN();

        System.out.println("====================================================================================");
        System.out.println("\nНайти число, цифры в котором идут в строгом порядке возростания. Если таких чисел несколько, найти первое из них.\n");
        numbersIncrease.increaseN();

        System.out.println("====================================================================================");
        System.out.println("\nНайти число, состоящее только из различных цифр. Если таких чисел несколько, найти первое из них.\n");
        differentNumbers.differenttN();


        System.out.println("====================================================================================");
        System.out.println("\nНайти количество чисел, содердащие только четные цифры, а среди них количество чисел с равным и неравным числом  четных и нечетных цифр.\n");
        evenNumbers.evenN();

        System.out.println("====================================================================================");
        System.out.println("\nВывести число от 1 до k в виде матрици NxN слева на право и сверху вниз .\n");
        matrix.matrixN();

        System.out.println("====================================================================================");
        System.out.println("\nВвести с консоли n-размерность матрицы а [n][n]. Задать значение элементов матрицы в интервали значений от n до n с помощью датчика случайных чисел.\n");
        matrixRandom.matrixR();

        System.out.println("====================================================================================");
        System.out.println("\nУпорядочить строки (столбцы) матрицы в порядке возростания значений.\n");
        increaseMatrix.increaseM();

        System.out.println("====================================================================================");
        System.out.println("\nВыполнить циклический сдвиг заданной матрицы на к позиций вправо (влево, вверх, вниз).\n");
        shiftMatrix.shiftM();

        System.out.println("====================================================================================");
        System.out.println("\nНайти и вывести наибольшее число возростающих (убывающих) элементов матрицы, идущих подряд.\n");
        largestNumberMatrix.largestN();

        System.out.println("====================================================================================");
        System.out.println("\nНайти сумму элементов матрицы, расположенных между первым и вторым положительными элементами каждой строки.\n");
        sumElements.sumE();

        System.out.println("====================================================================================");
        System.out.println("\nПовернуть матрицу на 90 (180, 270) градусов против часовой стрелки.\n");
        turnMatrix.turnM();

        System.out.println("====================================================================================");
        System.out.println("\nПостроить матрицу, вычитая из элементов каждой строки матрицы ее среднее арифметическое.\n");
        averageM.averageMatrix();

        System.out.println("====================================================================================");
        System.out.println("\nУплотнить матрицу, удаляя из нее строки и столбцы, заполненные нулями.\n");
        sealMatrix.sealM();

        System.out.println("====================================================================================");
        System.out.println("\nПреобразовать строки матрицы таким образом, чтобы элементы, равные нулю, располагались после всех остальных.\n");
        zeroElements.zeroE();



    }
}
